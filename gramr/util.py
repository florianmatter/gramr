# coding: utf8
from __future__ import unicode_literals
import re

from sqlalchemy import or_
from sqlalchemy.orm import joinedload
from markupsafe import Markup

import clld
from clld import interfaces
from clld import RESOURCES
from clld.web.util.htmllib import HTML, literal
from clld.web.util.downloadwidget import DownloadWidget
from clld.web.util.helpers import icon
from clld.db.meta import DBSession
from clld.db.models import common as models
from clld.web.adapters import get_adapter, get_adapters
from clld.lib.coins import ContextObject
from clld.lib import bibtex
from clld.lib import rdf
from clldmpg import cdstar

from clld.db.models import Source
import gramr.models as gramr_models
from gramr.models import Morpheme, Sentence, Language, Subdivision, TextSentence

def xify(text):
    ids = []
    for word in text.split(" "):
        ids.append(re.sub(r'([X])\1+', r'\1', re.sub("[^(\-|\=)|]", "X", word)))
    return " ".join(ids)
    
# # regex to match standard abbreviations in gloss units:
# # We look for sequences of uppercase letters which are not followed by a lowercase letter.
GLOSS_ABBR_PATTERN = re.compile(
    '(?P<personprefix>1|2|3)?(?P<abbr>[A-Z]+)(?P<personsuffix>1|2|3)?(?=([^a-z]|$))')
ALT_TRANSLATION_LANGUAGE_PATTERN = re.compile(r'((?P<language>[a-zA-Z\s]+):)')
#
def split_word(word):
    output = []
    char_list = list(word)
    for i, char in enumerate(char_list):
        if len(output) == 0 or (char in ["-","="] or output[-1] in ["-","="]):
            output.append(char)
        else:
            output[-1]+=char
    return output
    
# this is a clone of a function from clld/helpers.py, with morpheme links built in
# #
# # TODO: enumerate exceptions: 1SG, 2SG, 3SG, ?PL, ?DU
# #
def rendered_sentence(sentence, abbrs=None, fmt='long'):
    """Format a sentence as HTML."""
    if sentence.xhtml:
        return HTML.div(
            HTML.div(Markup(sentence.xhtml), class_='body'), class_="sentence")

    if abbrs is None:
        q = DBSession.query(models.GlossAbbreviation).filter(
            or_(models.GlossAbbreviation.language_pk == sentence.language_pk,
                models.GlossAbbreviation.language_pk == None)
        )
        abbrs = dict((g.id, g.name) for g in q)

    def gloss_with_tooltip(gloss):
        person_map = {
            '1': 'first person',
            '2': 'second person',
            '3': 'third person',
        }

        res = []
        end = 0
        for match in GLOSS_ABBR_PATTERN.finditer(gloss):
            if match.start() > end:
                res.append(gloss[end:match.start()])

            abbr = match.group('abbr')
            if abbr in abbrs:
                explanation = abbrs[abbr]
                if match.group('personprefix'):
                    explanation = '%s %s' % (
                        person_map[match.group('personprefix')], explanation)

                if match.group('personsuffix'):
                    explanation = '%s %s' % (
                        explanation, person_map[match.group('personsuffix')])

                res.append(HTML.span(
                    HTML.span(gloss[match.start():match.end()].lower(), class_='sc'),
                    **{'data-hint': explanation, 'class': 'hint--bottom'}))
            else:
                res.append(abbr)

            end = match.end()

        res.append(gloss[end:])
        return filter(None, res)

    def alt_translation(sentence):
        res = ''
        if sentence.jsondata.get('alt_translation'):
            text = sentence.jsondata['alt_translation']
            name = ''
            if ALT_TRANSLATION_LANGUAGE_PATTERN.match(text):
                name, text = [t.strip() for t in text.split(':', 1)]
                name = HTML.span(name + ': ')
            res = HTML.div(name, HTML.span(text, class_='translation'))
        return res

    units = []
    if sentence.analyzed and sentence.gloss:
        analyzed = sentence.analyzed
        glossed = sentence.gloss
        if sentence.markup_gloss:
            analyzed_words = sentence.markup_gloss
        else:
            analyzed_words = glossed
        # print(analyzed_words)
        for word, gloss, morph_id in zip(analyzed.split('\t'), glossed.split('\t'), analyzed_words.split("\t")):
            # print("%s, %s, %s" % (word, gloss, morph_id))
            obj_morphs = split_word(word)
            if sentence.markup_gloss:
                gloss_morphs = split_word(morph_id)
                for i, morph in enumerate(gloss_morphs):
                    if morph not in ["X","-","="]:
                        obj_morphs[i] = HTML.a(obj_morphs[i], href="/morpheme/%s" % morph.split(":")[0])
            parsed_word = HTML.text(*obj_morphs)
            gloss_morphs = re.split("[-|=]", gloss)
            units.append(HTML.div(
                HTML.div(parsed_word, class_='word'),
                HTML.div(*gloss_with_tooltip(gloss), **{'class': 'gloss'}),
                class_='gloss-unit'))
	
    my_file = ""
    for file in sentence._files:
        # print(file)
        if file.mime_type.startswith('audio'):
            my_file = get_audio_link(file)
                    
    return HTML.div(
        HTML.div(
            HTML.div(
                HTML.div(sentence.original_script, class_='original-script')
                if sentence.original_script else '',
                HTML.div(literal(sentence.markup_text or sentence.name), class_='object-language')
                if (not sentence.analyzed or sentence.name != sentence.analyzed.replace("\t", " ")) else '',
                HTML.div(*units, **{'class': 'gloss-box'}) if units else '',
                HTML.div(sentence.description, class_='translation')
                if sentence.description else '',
                alt_translation(sentence),
                my_file,
                class_='body',
            ),
            class_="sentence",
        ),
        class_="sentence-wrapper",
    )
    
def bitstream_url(filename):
    return "/audio/%s" % filename
            
def get_audio_link(filename):
    media_element = HTML.audio(
        "some sort of string",
        HTML.source(src=bitstream_url(filename), type="audio/x-wav"),
        controls="controls"
    )
    return HTML.div(media_element,
        # HTML.br(),
        HTML.a(
                HTML.span(
                    icon("icon-headphones"),
                    ' ' + "Download file",
                    class_='cdstar_link'),
                href=bitstream_url(filename))
        ,
        class_='audio_link',
        style='display: inline-block;vertical-align:middle;')

def generate_markup(non_f_str: str, html=True):
    
    ex_cnt = 0
    
    ex_nrs = {}
    
    if non_f_str is None:
        return ""
        
    substitutions = {
        "'(.*?)'": r"‘\1’",
        "morph:([a-z\_0-9]*)\|?([\u00BF-\u1FFF\u2C00-\uD7FF\(\)\w]*[\-\=]?)": r"{morph_lk('\1','\2')}",
        "morph_a:([a-z\_0-9]*)\|?([\u00BF-\u1FFF\u2C00-\uD7FF\(\)\w]*[\-\=]?)": r"{morph_audio_lk('\1','\2')}",
        "lg:([a-z]*)": r"{lang_lk('\1')}",
        "cons:([a-z\_]*)": r"{cons_lk('\1')}",
        "cogset:([a-z\_0-9]*)": r"{cogset_lk('\1')}",
        "crossref:([a-z\_0-9]*)": r"{chapter_lk('\1')}",
        "psrc:([a-z\_0-9\[\]\-]*)": r"{src_lk('\1', parens=True)}",
        "src:([a-z\_0-9\[\]\-]*)": r"{src_lk('\1')}",
        "ex:([a-z\_0-9\-A-Z]*)": r"{render_ex('\1')}",
        "exref:([a-z\_0-9\-A-Z]*)": r"{exref_lk('\1')}",
        "obj:([\w\-\(\)]*)": r"<i>\1</i>",
        "rc:([\w\-\(\)]*)": r"<i>*\1</i>",
        "audio:([a-z\_0-9]*)": r"{audio_lk('\1')}",
    }
    for orig, sub in substitutions.items():
        non_f_str = re.sub(orig, sub, non_f_str)
    
    def audio_lk(id):
        return '<audio controls="controls"><source src="%s" type="audio/x-wav"></source></audio>' % (bitstream_url(id))
        
    def cons_lk(shorthand):
        cons = DBSession.query(Construction).filter(Construction.id == shorthand)[0]
        return "<a href='/construction/%s'>%s</a>" % (shorthand, cons.language.name + " " + cons.name + " clause")
    
    def chapter_lk(id):
        chapter = DBSession.query(Subdivision).filter(Subdivision.id == id)[0]
        return "<a href='/subdivisions/%s'>%s</a>" % (id, chapter.name)
    
    def exref_lk(shorthand):
        nonlocal ex_nrs
        return "<a href=#%s>(%s)</a>" % (shorthand, ex_nrs[shorthand])
            
    def lang_lk(shorthand):
        # if shorthand in LANG_ABBREV_DIC.keys():
        #     return "<a href='/languages/%s'>%s</a>" % (LANG_ABBREV_DIC[shorthand]["ID"], LANG_ABBREV_DIC[shorthand]["name"])
        # elif shorthand in sampled_languages.keys():
        #     return "<a href='/languages/%s'>%s</a>" % (shorthand, sampled_languages[shorthand])
        # elif shorthand in language_names.keys():
        #     return language_names[shorthand]
        # elif shorthand in LANG_CODE_DIC.keys():
        #     return LANG_CODE_DIC[shorthand]["name"]
        # else:
        language = DBSession.query(Language).filter(Language.id == shorthand)[0]
        return "<a href='/languages/%s'>%s</a>" % (shorthand, language.name)            
            
    def cogset_lk(cogset_id, text=""):
        cogset = DBSession.query(Cognateset).filter(Cognateset.id == cogset_id)[0]
        if text == "":
            return "<i><a href='/cognateset/%s'>%s</a></i>" % (cogset_id, cogset)
        else:
            return "<i><a href='/cognateset/%s'>%s</a></i>" % (cogset_id, text)

    def src_lk(source_str, parens=False):
        bib_key = source_str.split("[")[0]
        source = DBSession.query(Source).filter(Source.id == bib_key)[0]
        if len(source_str.split("[")) > 1:
            pages = source_str.split("[")[1].split("]")[0]
            if not parens:
                return "<a href='/sources/%s'>%s</a>: %s" % (bib_key, source, pages.replace("--", "–"))
            else:
                return "(<a href='/sources/%s'>%s</a>: %s)" % (bib_key, source, pages.replace("--", "–"))
        else:
            if not parens:
                return "<a href='/sources/%s'>%s</a>" % (bib_key, source)
            else:
                return "(<a href='/sources/%s'>%s</a>)" % (bib_key, source)
    
    def morph_audio_lk(morph_id, form=""):
        if morph_id == "":
            return ""
        morph = DBSession.query(Morpheme).filter(Morpheme.id == morph_id)[0]
        if html:
            if form == "": form = morph.name
            return "<i><a href='/morpheme/%s'>%s</a></i>&nbsp;<audio id='%s_player' controls='controls'><source src='%s' type='audio/x-wav'></source></audio> <img class='playbutton' onclick='%s_player.play()' src='/static/play.png'>" % (morph_id, morph_id, morph_id, bitstream_url(morph_id), form)
        else:
            if form == "": form = morph.name
            return "\\obj{%s}" % form
            
    def morph_lk(morph_id, form=""):
        if morph_id == "":
            return ""
        morph = DBSession.query(Morpheme).filter(Morpheme.id == morph_id)[0]
        if html:
            if form == "": form = morph.name
            return "<i><a href='/morpheme/%s'>%s</a></i>" % (morph_id, form)
        else:
            if form == "": form = morph.name
            return "\\obj{%s}" % form
        
    def render_ex(ex_id):
        nonlocal ex_cnt
        nonlocal ex_nrs
        ex_cnt += 1
        example = DBSession.query(Sentence).filter(Sentence.id == ex_id)[0]
        text_id = example.text_assocs[0].text.id
        ex_nrs[example.id] = ex_cnt
        # print(ex_nrs)
        return """
            <blockquote style='margin-top: 5px; margin-bottom: 5px'>
            (<a href='/example/%s' name=%s>%s</a>) <a href=/text/%s#%s>%s</a>
                %s
            </blockquote>""" % (example.id,example.id,ex_cnt,
                            text_id, ex_id, ex_id,
                            # lang_lk(example.language.id),
                            # src_lk("%s[%s]" % (example.references[0].source.id, example.references[0].description)),
                            rendered_sentence(example)
                        )
    
    result = eval(f'f"""{non_f_str}"""')
    return result