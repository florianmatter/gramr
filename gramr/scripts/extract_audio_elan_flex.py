from pydub import AudioSegment
from pycldf import Wordlist
import csv
import os
#ffmpeg 4.2.2_2 -> 4.3.1
texts = {}
# with open('mapudungun_texts.csv') as csvfile:
with open('vamale_texts.csv') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        texts[row["ID"]] = dict(row)

lg_data = Wordlist.from_metadata("/Users/florianm/Desktop/vamale_gramr/vamale_data.json")
# lg_data = Wordlist.from_metadata("./mapudungun_data.json")

media_folder = "./audio"
full_media_folder = "/Users/florianm/Desktop/vamale_gramr"
# full_media_folder = "../full_audio"

full_audios = {}
for text_id, data in texts.items():
    if data["Filename"] != "":
        file_name = "%s/%s" % (full_media_folder, data["Filename"])
        try:
            with open(file_name, 'r') as fh:
                full_audios[text_id] = AudioSegment.from_wav(file_name)
        except FileNotFoundError:
            print("Missing file for text %s: %s" % (text_id, file_name))

for i, row in enumerate(lg_data["ExampleTable"]):
    ex_id = row["ID"]
    if row["Time_Start"] != None:
        file_destination = "%s/%s.mp3" % (media_folder, ex_id)
        begin = int(row["Time_Start"])
        sentence = row["Name"]
        end = int(row["Time_End"])
        if row["Text_ID"] in full_audios.keys():
            if os.path.isfile(file_destination):
                print("File %s already exists, skipping…" % file_destination, end="\r")
            else:
                print("Adding file %s…" % file_destination, end="\r")
                segment = full_audios[row["Text_ID"]][begin:end]
                segment.export(file_destination, format="mp3")
print("")