import csv
import re
import pynterlinear
import datetime
from string import Template
import pypandoc
import os

reader = csv.DictReader(open("vamale_texts_edited.csv"))
example_data = {}
for row in reader:
    example_data[row["Example_ID"]] = row
    
reader = csv.DictReader(open("vamale_dic_from_lift.csv"))
lexicon = {}
for row in reader:
    lexicon[row["ID"]] = row["Form"]
    
def render_markdown(non_f_str: str):    
    # print(non_f_str)
    if non_f_str is None:
        return ""
        
    substitutions = {
        # "'(.*?)'": r"{render_shortcut('\1', 'qu')}",
        "morph:([a-z\_0-9A-Z]*)\|?([\u00BF-\u1FFF\u2C00-\uD7FF\(\)\w]*[\-\=]?)": r'{morph_lk("\1")}',
        "morph_a:([a-z\_0-9A-Z]*)\|?([\u00BF-\u1FFF\u2C00-\uD7FF\(\)\w]*[\-\=]?)": r'{morph_lk("\1")}',
        "lg:([a-z]*)": r"{lang_lk('\1')}",
        "crossref:([a-z\_0-9]*)": r"{render_shortcut('sec:\1', 'Cref')}",
        "psrc:([a-z\_0-9\[\]\-]*)": r"{src_lk('\1', parencite=True)}",
        "src:([a-z\_0-9\[\]\-]*)": r"{src_lk('\1')}",
        "ex:([a-z\_0-9\-A-Z]*)": r"{render_ex('\1')}",
        "exref:([a-z\_0-9\-A-Z]*)": r"{render_shortcut('\1', 'exref')}",
        "obj:([\w\-\(\)]*)": r"{render_shortcut('\1', 'obj')}",
        "rc:([\w\-\(\)]*)": r"{render_shortcut('\1', 'rc')}",
        # "audio:([a-z\_0-9]*)": "Audio",
    }

    for orig, sub in substitutions.items():
        # if orig=="'(.*?)'" and "blowpipe" in non_f_str:
        #     print(non_f_str)
        non_f_str = re.sub(orig, sub, non_f_str)
        # if orig=="'(.*?)'" and "blowpipe" in non_f_str:
        #     print(non_f_str)
    
    class DeltaTemplate(Template):
        delimiter = "%"
        
    def get_time(str):
        fmt = '%H:%M:%S'
        tdelta = datetime.timedelta(seconds=int(str)/1000)
        d = {"D": tdelta.days}
        hours, rem = divmod(tdelta.seconds, 3600)
        minutes, seconds = divmod(rem, 60)
        d["H"] = '{:02d}'.format(hours)
        d["M"] = '{:02d}'.format(minutes)
        d["S"] = '{:02d}'.format(seconds)
        t = DeltaTemplate(fmt)
        return t.substitute(**d)
        
    def render_shortcut(form, shortcut):
        return("\%s{%s}" % (shortcut, form))
        
    def lang_lk(shorthand):
        return shorthand     

    def src_lk(source_str, parencite=False):
        if not parencite:
            citestring = "textcite"
        else:
            citestring = "parencite"
        bib_key = source_str.split("[")[0]
        if len(source_str.split("[")) > 1:
            pages = source_str.split("[")[1].split("]")[0]
            return "\\%s[%s]{%s}" % (citestring, pages, bib_key)
        else:
            return "\\%s{%s}" % (citestring, bib_key)

    def morph_lk(morph_id):
        return render_shortcut(lexicon[morph_id], "obj") 
                
    def render_ex(ex_id):
        examples = []
        for key in [ex_id]:
            ex = example_data[key]
            ex_hash = {
                "id": ex["Example_ID"],
                "obj": ex["Segmentation"],
                # "language": ex["Language_ID"],
                "gloss": ex["Gloss"],
                "trans": ex["Translation"],
                "surface": ex["Sentence"],
                "speaker": ex["Speaker"],
                "text_id": ex["Text_ID"],
                "part": ex["Part"],
            }
            if ex["Time_Start"] != "":
                ex_hash["start"] = get_time(ex["Time_Start"])
                ex_hash["end"] = get_time(ex["Time_End"])
            examples.append(ex_hash)
            return "\n\n" + pynterlinear.convert_to_expex(examples, from_corpus=True, for_beamer=False) + "\n"

    result = eval(f'f"""{non_f_str}"""')
    return result

output = r"""\input{gramr_latex_config.tex}
\newGlossingAbbrev{acomp}{accomplishment}
\newGlossingAbbrev{subj}{subject}
\begin{document}
\tableofcontents
"""
sections = {}
with open('vamale_sections.csv') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        sections[row["ID"]] = row["Title"]

toc = []
level_mapping = {
    "1": "chapter",
    "2": "section",
    "3": "subsection",
    "4": "subsubsection"
}

def escape_f(text):
    return text.replace("{", "{{").replace("}", "}}").replace("\\", "\\\\")

def unescape(text):
    return text.replace("{[}", "[").replace("{]}", "]").replace("{[}", "[").replace("{]}", "]")

def replace_graphics_path(text):
    return text.replace("includegraphics{/static/", "includegraphics[width=0.6\\textwidth]{gramr/static/")
    
with open('vamale_sections.csv') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        if row["ID"] != "":
            output += "\\%s{%s}\n\\label{sec:%s}\n\n" % (level_mapping[row["Level"]], sections[row["ID"]], row["ID"])
            text = open("vamale_sections/%s.txt" % row["ID"],"r").read()
            text = pypandoc.convert_text(text, 'tex', format='md')
            text = escape_f(text)
            text = unescape(text)
            text = text.replace("morph\\\\_a", "morph_a")
            text = render_markdown(text)
            text = replace_graphics_path(text)
            output += text + "\n"
        else:
            output += "\\%s{%s}\n\n" % (level_mapping[row["Level"]], row["Title"])
output += "\\glossingAbbrevsList\n\\printbibliography\n\\end{document}"
f = open("latex_version.tex", "w")
f.write(output)
f.close()
