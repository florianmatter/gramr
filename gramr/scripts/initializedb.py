from __future__ import unicode_literals
import sys

import re
from clld.scripts.util import initializedb, Data
from clld.db.meta import DBSession
from clld.db.models import common
from clld.lib.bibtex import EntryType, unescape
from pycldf import Wordlist
from nameparser import HumanName
import gramr
from gramr import util
from gramr import models
import csv
import os
import lorem

import importlib.util
spec = importlib.util.spec_from_file_location("morph_merge", "/Users/florianm/Dropbox/Stuff/development/morph_merge/__init__.py")
mm = importlib.util.module_from_spec(spec)
spec.loader.exec_module(mm)

#This functions returns a author-year style reference from the bibtex file, based on phoible  (https://github.com/clld/phoible/blob/59cc304235a0526caff7c6777809a7a7b239ae2a/phoible/scripts/initializedb.py#L37)
def get_source_name(source):
    year = source.get('year', 'nd')
    fields = {}
    jsondata = {}
    eds = ''
    authors = source.get('author')
    if not authors:
        authors = source.get('editor', '')
        if authors:
            eds = ' (eds.)'
    if authors:
        authors = unescape(authors).split(' and ')
        etal_string = ""
        if len(authors) > 2:
            authors = authors[:1]
            etal_string = " et al."

        authors = [HumanName(a) for a in authors]
        authors = [n.last or n.first for n in authors]
        authors = '%s%s' % (' and '.join(authors), eds)
        authors += etal_string

        return ('%s %s' % (authors, year)).strip()
              
def main(args):
    data = Data()
    
    print("Setting up dataset…")
    dataset = common.Dataset(id=gramr.__name__,
        name="gramr",
        domain="gramr-demo.herokuapp.com/",
        description="Gramr demo",
        publisher_name="Florian Matter",
        publisher_url="http://www.isw.unibe.ch/ueber_uns/personen/ma_matter_florian/index_ger.html",
        publisher_place="University of Bern",
        license="http://creativecommons.org/licenses/by/4.0/",
        contact="florian.matter@isw.unibe.ch"
        )
        
    DBSession.add(dataset)
    
    print("Adding contributors…")
    c = common.Contributor(id="fm",name="Florian Matter")
    dataset.editors.append(common.Editor(contributor=c, ord=1, primary=True))
    
    print("Adding chapters…")

    with open('vamale_sections.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        structure_walk = {}
        counters = {
            1: 0,
            2: 0,
            3: 0
        }
        
        for row in reader:            
            level = int(row["Level"])

            new_entry = data.add(models.Subdivision,
            row["ID"],
            id=row["ID"],
            name=row["Title"],
            level=level
            )
            
            counters[level] += 1
            if level == 1:
                new_entry.number = "%s." % (counters[1])
                counters[2] = 0
                counters[3] = 0
            if level == 2:
                new_entry.chapter=structure_walk[level-1]
                new_entry.number = "%s.%s." % (counters[1], counters[2])
                counters[3] = 0
            if level == 3:
                new_entry.chapter=structure_walk[level-2]
                new_entry.section=structure_walk[level-1]
                new_entry.number = "%s.%s.%s." % (counters[1], counters[2], counters[3])
                
            structure_walk[level] = new_entry
        
    
    #Get lexicon and examples from CLDF database
    lg_data = Wordlist.from_metadata("vamale_data.json")
                    
    if lg_data.sources:
        print("Adding sources…")
        for i, src in enumerate(lg_data.sources.items()):
            for invalid in ["isbn", "part", "institution", "doi"]:
                if invalid in src:
                    del src[invalid]
            data.add(
                common.Source,
                src.id,
                id=src.id,
                name=get_source_name(src),
                description=src.get("title", src.get("booktitle")).replace("{","").replace("}",""),
                bibtex_type=getattr(EntryType, src.genre, EntryType.misc),
        **src)
            print(src.id)
        
    print("Adding speakers…")
    speaker_names = {}
    with open('vamale_speakers.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            name = row["Given_name"] + " " + row["Family_name"]
            speaker_names[row["ID"]] = name
            data.add(models.Speaker,
                row["ID"],
                id=row["ID"],
                name=name,
                family_name=row["Family_name"],
                given_name=row["Given_name"],
                year_of_birth=row["Year_of_birth"],
                gender=row["Gender"],
                description=row["Comment"],
            )
                
    print("Adding texts…")

    #Read overview of texts in corpus
    texts = {}
    with open('vamale_texts.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            texts[row["ID"]] = dict(row)
            
    for key, values in texts.items():
        data.add(models.Text,
            values["ID"],
            id=values["ID"],
            name=values["Title"],
            description=values["Description"],
            markup_description=util.generate_markup(values["Description"]),
            text_type=values["Type"]
        )
    
    print("Adding Languages…")
    data.add(
        common.Language,
        "arn",
        id="arn",
        name="Mapudungun",
        latitude=-38.7392,
        longitude=-71.277,
    )
    
    data.add(
        common.Language,
        "mkt",
        id="mkt",
        name="Vamale",
        latitude=-20.77,
        longitude=165.04
    )
    
    print("Adding morphemes…")
    morpheme_info = {}
    with open('vamale_morpheme_info.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            morpheme_info[row["ID"]] = dict(row)

    for row in lg_data["FormTable"]:
        form = mm.merge_allomorphs("; ".join(row["Form"])).replace("; ", "/")
        new_morph = data.add(models.Morpheme,
            row["ID"],
            language=data["Language"][row["Language_ID"]],
            name=form,
            id=row["ID"],
            description="; ".join(row["Parameter_ID"])
        )
        desc = ""
        if row["ID"] in morpheme_info.keys():
            desc += "\n" + morpheme_info[row["ID"]]["Description"]
            desc = util.generate_markup(desc)
            new_morph.markup_description = desc
            for loc_ref in morpheme_info[row["ID"]]["References"].split("; "):
                data.add(models.MorphemeChapter,
                    row["ID"] + "_" + loc_ref,
                    unit = new_morph,
                    chapter = data["Subdivision"][loc_ref]
                )
            
        morpheme_function = "; ".join(row["Parameter_ID"])
        if morpheme_function not in data["Meaning"].keys():
            data.add(models.Meaning,
                morpheme_function,
                id=row["ID"],
                name=morpheme_function
            )
        data.add(common.UnitValue,
            row["ID"],
            id=row["ID"],
            name=row["ID"],
            unit=data["Morpheme"][row["ID"]],
            unitparameter=data["Meaning"][morpheme_function]
        )
    
    def clldify_glosses(gloss_line):
        output = gloss_line
        output = re.sub(r"(\d)([A-Z])", r"\1.\2", output)
        return output
        
    print("Adding examples…")
    for row in lg_data["ExampleTable"]:
        if row["Analyzed_Word"] == ["MISSING"]:
            obj_line = None
            gloss_line = None
        else:
            obj_line = "\t".join(row["Analyzed_Word"])
            gloss_line = clldify_glosses("\t".join(row["Gloss"]))
        new_ex = data.add(common.Sentence,
                            row["ID"],
                            id=row["ID"],
                            name=row["Name"],
                            description=row["Translated_Text"],
                            analyzed=obj_line,
                            gloss=gloss_line,
                            language=data["Language"][row["Language_ID"]],
                            markup_gloss="\t".join(row["Morph_IDs"].split(" ")),
        )
        for word in row["Morph_IDs"].split(" "):
            morph_ids = util.split_word(word)
            for morph_id in morph_ids:
                if morph_id in ["X","-","="]:
                    continue
                unitsentence_key = "{0}-{1}".format(morph_id.replace(".","-"),row["ID"])
                if unitsentence_key in data["UnitSentence"].keys():
                    continue
                if morph_id not in data["Morpheme"].keys():
                    print("Warning: Example %s illustrates unknown morpheme %s" % (row["ID"], morph_id))
                elif data["Morpheme"][morph_id].language != data["Sentence"][row["ID"]].language:
                    print("Warning: The %s example %s claims to contain the %s morpheme %s." % (
                        data["Sentence"][row["ID"]].language,
                        row["ID"],
                        data["Morpheme"][morph_id].language,
                        data["Morpheme"][morph_id]
                    )
                    )
                data.add(models.UnitSentence,
                unitsentence_key,
                sentence=data["Sentence"][row["ID"]],
                unit=data["Morpheme"][morph_id],
                )
        
        DBSession.flush()
        
        if row["Text_ID"] in texts.keys():
            text_sentence = models.TextSentence(
                row["ID"],
                text_pk = data["Text"][row["Text_ID"]].pk,
                sentence_pk = new_ex.pk,
                part_no = row["Part"]
            )
            DBSession.add(text_sentence)
        if row["Speaker"] in speaker_names.keys():
            speaker_sentence = models.SpeakerSentence(
                row["ID"],
                speaker_pk = data["Speaker"][row["Speaker"]].pk,
                sentence_pk = new_ex.pk
            )
            DBSession.add(speaker_sentence)

    print("Adding morpheme files…")
    audio_files = []
    for filename in os.listdir("audio"):
        audio_files.append(filename.split(".")[0])

    for row in lg_data["FormTable"]:
        if row["ID"] in audio_files:
            unit_file = common.Unit_files(
                object_pk = data["Morpheme"][row["ID"]].pk,
                name = "%s" % row["ID"],
                id = "%s" % row["ID"],
                mime_type = "audio/mp3",
            )
            DBSession.add(unit_file)
            DBSession.flush()
            DBSession.refresh(unit_file)
        
    print("Adding sentence files…")
    for row in lg_data["ExampleTable"]:
        if row["Time_Start"] != None:
            sentence_file = common.Sentence_files(
                object_pk = data["Sentence"][row["ID"]].pk,
                name = "%s" % row["ID"],
                id = "%s" % row["ID"],
                mime_type = "audio/mp3",
            )
            DBSession.add(sentence_file)
            DBSession.flush()
            DBSession.refresh(sentence_file)
    
    print("Adding section contents…")
    with open('vamale_sections.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            section_text = util.generate_markup(open("vamale_sections/%s.txt" % row["ID"],"r").read())
            data["Subdivision"][row["ID"]].markup_description = section_text + "\n\n" + lorem.text()

def prime_cache(args):
    """If data needs to be denormalized for lookup, do that here.
    This procedure should be separate from the db initialization, because
    it will have to be run periodiucally whenever data has been updated.
    """

if __name__ == '__main__':
    initializedb(create=main, prime_cache=prime_cache)
    sys.exit(0)