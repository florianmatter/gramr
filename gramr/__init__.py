import os
from pyramid.config import Configurator
from pyramid.response import Response, FileResponse
# we must make sure custom models are known at database initialization!
from gramr import models
from gramr.interfaces import IText, ISpeaker, ISubdivision

_ = lambda s: s
_('Unit')
_('Units')
_('Sentence')
_('Sentences')
_('Subdivisions')

def get_audio_path(audio_id):
    if "." in audio_id and os.path.isfile("audio/%s" % audio_id):
        return "audio/%s" % audio_id
    else:
        for suffix in audio_suffixes:
            test_path = "audio/%s%s" % (audio_id, suffix)
            if os.path.isfile(test_path):
                return test_path
    return None
    
audio_suffixes = [".mp3", ".wav"]
def audio_view(request):
    audio_id = request.matchdict["audio_id"]
    print("Audio %s requested" % audio_id)
    audio_path = get_audio_path(audio_id)
    if audio_path:
        response = FileResponse(audio_path, request=request,cache_max_age=86400)
        return response
    else:
        error = "Audio %s requested but not found" % audio_id
        print(error)
        return Response('<body>%s</body>' % error)
    
def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    settings['route_patterns'] = {
        'texts': '/texts',
        'text': '/text/{id:[^/\.]+}',
        'units': '/morphemes',
        'unit': '/morpheme/{id:[^/\.]+}',
        'sentences': '/examples',
        'sentence': '/example/{id:[^/\.]+}',
    }
    
    config = Configurator(settings=settings)
    config.include('clld.web.app')
    config.register_resource(
        'subdivision', models.Subdivision, ISubdivision, with_index =True, with_detail=True)
    config.register_resource(
        'text', models.Text, IText, with_index =True, with_detail=True)
    config.register_resource(
        'speaker', models.Speaker, ISpeaker, with_index =True, with_detail=True)
    
    config.add_route('audio_route', '/audio/{audio_id}')
    config.add_view(audio_view, route_name='audio_route')
    
    app = config.make_wsgi_app()
    return config.make_wsgi_app()
    