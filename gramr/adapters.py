from clld.web.adapters.download import Download
from clld.db.models.common import Dataset

class PdfVersion(Download):  # pragma: no cover
    ext = "pdf"
    description = "PDF version of the grammatical description"
    
    def asset_spec(self, req):
        return "gramr:static/download/latex_version.pdf"
        
    # def asset_spec(self, req):
    #     return "download/latex_version.pdf"
            
    # def url(self, req):
    #     return()
        
        
def includeme(config):
    config.register_download(PdfVersion(Dataset, "gramr"))
