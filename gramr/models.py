from zope.interface import implementer
from sqlalchemy import (
    Column,
    String,
    Unicode,
    Integer,
    Boolean,
    ForeignKey,
    UniqueConstraint,
    Float,
    Date,
)
from sqlalchemy.orm import relationship, backref
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.ext.hybrid import hybrid_property
from clld import interfaces
from clld.db.meta import Base, CustomModelMixin, PolymorphicBaseMixin
from clld.db.models import UnitParameter, Unit, Value, Parameter, ValueSet, UnitValue, Sentence, IdNameDescriptionMixin, HasSourceMixin, Language
from gramr.interfaces import IVillage, IFile, IAudio, IText, ISpeaker, ISubdivision

@implementer(interfaces.IUnitParameter)
class Meaning(CustomModelMixin, UnitParameter):
    pk = Column(Integer, ForeignKey('unitparameter.pk'), primary_key=True)
    form = Column(String)

@implementer(interfaces.ILanguage)
class Languoid(Language, PolymorphicBaseMixin):
    pk = Column(Integer, ForeignKey('language.pk'), primary_key=True)
    status = Column(Unicode)
    
@implementer(interfaces.IParameter)
class CognateSet(CustomModelMixin, Parameter, HasSourceMixin):
    pk = Column(Integer, ForeignKey('parameter.pk'), primary_key=True)

@implementer(interfaces.IUnit)
class Morpheme(CustomModelMixin, Unit):
    pk = Column(Integer, ForeignKey('unit.pk'), primary_key=True)

@implementer(interfaces.IValue)
class Counterpart(CustomModelMixin, Value):
    pk = Column(Integer, ForeignKey('value.pk'), primary_key=True)
    morpheme_pk = Column(Integer, ForeignKey('morpheme.pk'))
    morpheme = relationship(Morpheme, backref='counterparts')

@implementer(IText)
class Text(Base, IdNameDescriptionMixin):
    pk = Column(Integer, primary_key=True)
    text_type = Column(Unicode())
    
@implementer(ISpeaker)
class Speaker(Base, IdNameDescriptionMixin):
    pk = Column(Integer, primary_key=True)
    
    year_of_birth = Column(Integer)    
    gender = Column(String)
    family_name = Column(String)
    given_name = Column(String)
    latitude = Column(Float())
    longitude = Column(Float())
        
class SpeakerSentence(Base, PolymorphicBaseMixin):
    __table_args__ = (UniqueConstraint('speaker_pk', 'sentence_pk'),)
    
    speaker_pk = Column(Integer, ForeignKey('speaker.pk'), nullable=False)
    sentence_pk = Column(Integer, ForeignKey('sentence.pk'), nullable=False)
    speaker = relationship(Speaker, innerjoin=True, backref='sentences', order_by=Sentence.id)
    sentence = relationship(Sentence, innerjoin=True, backref='speaker_assocs')
    
class TextSentence(Base, PolymorphicBaseMixin):
    __table_args__ = (UniqueConstraint('text_pk', 'sentence_pk'),)
    
    text_pk = Column(Integer, ForeignKey('text.pk'), nullable=False)
    sentence_pk = Column(Integer, ForeignKey('sentence.pk'), nullable=False)
    text = relationship(Text, innerjoin=True, backref='sentences', order_by=Sentence.id)
    sentence = relationship(Sentence, innerjoin=True, backref='text_assocs')
    part_no = Column(Integer)
        
@implementer(ISubdivision)
class Subdivision(Base, IdNameDescriptionMixin):
    pk = Column(Integer, primary_key=True)
    number = Column(Unicode())
    level = Column(Integer)
    chapter_id = Column(Integer, ForeignKey("subdivision.pk"))
    chapter = relationship("Subdivision", remote_side=[pk], backref='sections', foreign_keys=[chapter_id])
    section_id = Column(Integer, ForeignKey("subdivision.pk"))
    section = relationship("Subdivision", remote_side=[pk], backref='subsections', foreign_keys=[section_id])
    
class MorphemeChapter(Base, PolymorphicBaseMixin):
    
    unit_pk = Column(Integer, ForeignKey('unit.pk'), nullable=False)
    chapter_pk = Column(Integer, ForeignKey('subdivision.pk'), nullable=False)
    unit = relationship(Morpheme, innerjoin=True, backref='morphemechapters')
    chapter = relationship(Subdivision, innerjoin=True)
    
class UnitSentence(Base, PolymorphicBaseMixin):

    """Association between morphemes and sentences given as explanation of a value."""

    __table_args__ = (UniqueConstraint('unit_pk', 'sentence_pk'),)

    unit_pk = Column(Integer, ForeignKey('unit.pk'), nullable=False)
    sentence_pk = Column(Integer, ForeignKey('sentence.pk'), nullable=False)
    description = Column(Unicode())

    unit = relationship(Unit, innerjoin=True, backref='sentence_assocs')
    sentence = relationship(Sentence, innerjoin=True, backref='unit_assocs', order_by=Sentence.id)
    
@implementer(IAudio)
class Audio(Base, IdNameDescriptionMixin):
    # id: slug of filename
    # name: summary description
    # description: category
    duration = Column(Float)

    def get_file(self, subtype):
        for f in self.files:
            if f.mime_type == 'audio/' + subtype:
                return f
                    
@implementer(IFile)
class File(Base, IdNameDescriptionMixin):
    # id: md5 checksum
    # name: filename
    size = Column(Integer)
    duration = Column(Float)
    date_created = Column(Date)
    mime_type = Column(Unicode)

    audio_pk = Column(Integer, ForeignKey('audio.pk'))
    audio = relationship(Audio, backref='files')

    @property
    def maintype(self):
        return self.mime_type.split('/')[0]

    @property
    def subtype(self):
        return self.mime_type.split('/')[1]