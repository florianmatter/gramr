from pathlib import Path

from clld.web.assets import environment

import gramr


environment.append_path(
    Path(gramr.__file__).parent.joinpath('static').as_posix(),
    url='/gramr:static/')
environment.load_path = list(reversed(environment.load_path))
