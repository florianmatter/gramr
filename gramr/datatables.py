from clld.web.datatables.base import DataTable, Col, LinkCol, DetailsRowLinkCol
from gramr.models import Subdivision, Text
from sqlalchemy import or_
from clld.db.util import get_distinct_values

class StructureCol(Col):
    __kw__ = dict(sClass='right', sTitle='§', bSearchable=False)
    
class Texts(DataTable):

    def col_defs(self):
        return [
            LinkCol(self, 'name'),
            Col(self, 'type', get_obj=lambda i: i.text_type, choices=get_distinct_values(Text.text_type), model_col=Text.text_type)
        ]
        
        
class Subdivisions(DataTable):
    __constraints__ = [Subdivision]

    def base_query(self, query):

        if self.subdivision:
            query = query.filter(or_(self.subdivision.pk == Subdivision.section_id, self.subdivision.pk == Subdivision.chapter_id))

        return query
    
    def col_defs(self):

        base = [
            StructureCol(
                self,
                'number',
            ),
            LinkCol(self, 'name'),
        ]
        
        if self.subdivision:
            level = self.subdivision.level
        else:
            level = 0
        
        if level < 1:
            base.append(LinkCol(self, 'chapter', get_obj=lambda i: i.chapter, bSortable=False, bSearchable=False))
        
        if level < 2:
            base.append(LinkCol(self, 'section', get_obj=lambda i: i.section, bSortable=False, bSearchable=False))
            
        return base
        
    
def includeme(config):
    config.register_datatable('texts', Texts)
    config.register_datatable('subdivisions', Subdivisions)
    pass
