<%inherit file="../${context.get('request').registry.settings.get('clld.app_template', 'app.mako')}"/>
<%namespace name="util" file="../util.mako"/>
<%namespace name="ex_util" file="../ex_util.mako"/>
<%! active_menu_item = "texts" %>


<h3>${_('Text')} “${ctx.name}”</h3>

% if ctx.markup_description:
${h.text2html(h.Markup(ctx.markup_description), mode='p')}
% endif

<% ex_cnt = 0 %>
% for s in ctx.sentences:
	<% speaker = ""%>
	%for speaker_sentence in s.sentence.speaker_assocs:
		<% speaker = h.link(request, speaker_sentence.speaker)%>
	%endfor
	<% ex_cnt += 1 %>
	<% my_file = "" %>
	% for file in s.sentence._files:
		% if file.mime_type.startswith('audio'):
			<% my_file = u.get_audio_link(file) %>
		% endif
	% endfor
	<blockquote style='margin-top: 5px; margin-bottom: 5px'>
	<% ex_id = s.sentence.id %>
	(${h.link(request, s.sentence, label=ex_cnt, name = ex_id)}) ${speaker}
	${u.rendered_sentence(s.sentence, fmt=fmt)}
	</blockquote>
	<br>
% endfor