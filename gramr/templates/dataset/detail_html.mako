<%inherit file="../home_comp.mako"/>
<%namespace name="util" file="../util.mako"/>
<% import markdown%>
<% import csv%>
<%def name="sidebar()">
##    <div class="well">
##        <h3>Sidebar</h3>
##        <p>
##            Content
##        </p>
##    </div>
</%def>

<h2>gramr: Demo</h2>

<p class="lead">
    This is a <b>demo version</b> of an interactive and data-rich online grammar with <a href="/texts">texts</a> and a <a href="/morphemes">dictionary</a>.
	The grammar is browsable <a href="/subdivisions">here</a>.
	For demonstrating the functionality, <a href="https://glottolog.org/resource/languoid/id/vama1243">Vamale</a> data is used, courtesy of <a href="https://www.isw.unibe.ch/ueber_uns/personen/ma_rohleder_jean/index_ger.html">Jean Rohleder</a>.
	There is also a <a href="/static/download/latex_version.pdf">PDF version</a> in a more traditional format with identical contents.
	<b>The data, especially the contents of the grammar, is purely for demonstration purposes and should not be taken seriously.</b>
</p>

<p>
	${h.Markup(
			markdown.markdown("""The source code for this demo can be found [here](https://gitlab.com/florianmatter/gramr/).
The dictionary and text data you see was extracted from a [FLEx](https://software.sil.org/fieldworks/) database which was fed texts transcribed in [ELAN](https://archive.mpi.nl/tla/elan), a standard setup in field linguistics.
The data was converted to the [CLDF](https://cldf.clld.org/) format using [a set of Python scripts](https://gitlab.com/florianmatter/cldflex/).""", extensions=['tables'])
		)
	}


</p>