<%inherit file="../${context.get('request').registry.settings.get('clld.app_template', 'app.mako')}"/>
<%namespace name="util" file="../util.mako"/>
<%namespace name="ex_util" file="../ex_util.mako"/>
<% import datetime %>
<%! active_menu_item = "speakers" %>

<h3>${_('Speaker')} ${ctx.name}</h3>

<table class="table table-nonfluid">
	<thead><tr></tr></thead>
	<tbody>
	<tr>
	<td>Age:</td>
	<td>${datetime.datetime.now().year-ctx.year_of_birth} (b. ${ctx.year_of_birth})</td>
	</tr>
	<tr>
	<td>Gender:</td>
	<td>${ctx.gender}</td>
	</tr>
</tbody>
</table>

% if ctx.description:
<p>${ctx.description}</p>
% endif