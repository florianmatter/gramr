<%inherit file="../${context.get('request').registry.settings.get('clld.app_template', 'app.mako')}"/>
<%namespace name="util" file="../util.mako"/>
<%! active_menu_item = "speakers" %>
<%block name="title">${_('Speakers')}</%block>

<h2>${title()}</h2>

% if map_ or request.map:
${(map_ or request.map).render()}
% endif

<div>
    ${ctx.render()}
</div>
