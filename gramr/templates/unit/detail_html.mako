<%inherit file="../${context.get('request').registry.settings.get('clld.app_template', 'app.mako')}"/>
<%namespace name="util" file="../util.mako"/>
<%namespace name="ex_util" file="../ex_util.mako"/>
<%! active_menu_item = "units" %>


<h3>${_('Entry')} <i>${ctx.name}</i></h3>

## <p>
##     ${ctx.description}
## </p>

<table class="table-detail">
	<thead>
		<th></th>
	</thead>
    <tbody>
	<tr>
	<td>Form(s):</td>
	<td>${ctx.name}</td>
	</tr>
	<tr>
	<td>Language:</td>
	<td>${h.link(request, ctx.language)}</td>
	</tr>
	<tr>
	% if ctx.morphemechapters:
		<td>Discussed in:</td>
		<td>
		<ul class="inline">
			% for c in ctx.morphemechapters:
				<li>${h.link(request, c.chapter)}</li>
			% endfor
		</ul>
		</td>
	% endif
##	<td>Cognate set(s):</td>
##	<td>
##	% if ctx.counterparts:
##		<ul class="inline">
##			% for c in ctx.counterparts:
##				<li>${h.link(request, c.valueset.parameter)}</li>
##			% endfor
##		</ul>
##	% endif
##	</td>
	</tr>
    </tbody>
</table>

% if ctx.markup_description:
${h.text2html(h.Markup("Comments: " + ctx.markup_description), mode='p')}
% endif

<dl>
% for key, objs in h.groupby(ctx.data, lambda o: o.key):
<dt>${key}</dt>
    % for obj in sorted(objs, key=lambda o: o.ord):
    <dd>${obj.value}</dd>
    % endfor
% endfor
</dl>

<h4>${_('Meanings:')}</h4>
<dl>
    % for i, value in enumerate(ctx.unitvalues):
		<div style="clear: right;">
		    <ul class="nav nav-pills pull-right">
		        <li><a data-toggle="collapse" data-target="#s${i}">Show/hide details</a></li>
		    </ul>
			<h4>
				<dt>‘${h.link(request, value.unitparameter)}’</dt>
			</h4>
		</div>
    ##<dd>${h.link(request, value)}</dd>
    % endfor
</dl>

<dl>
		    <div id="s${i}" class="collapse in">
				${ex_util.sentences(ctx)}
		    </div>
</dl>

% for file in ctx._files:
    % if file.mime_type.startswith('audio'):
<p>
${u.get_audio_link(file)}
<p>
    % endif
% endfor