<%inherit file="../${context.get('request').registry.settings.get('clld.app_template', 'app.mako')}"/>
<%namespace name="util" file="../util.mako"/>
<%! active_menu_item = "subdivisions" %>
<%block name="title">${_('Subdivisions')}</%block>

<h2>${title()}</h2>
This is an attempt at displaying the contents of the grammatical description in a mixture of traditional and more database-like form.
A more traditional form with identical contents can be downloaded <a href="/static/download/latex_version.pdf">here</a>.
<div>
	${request.get_datatable('subdivisions', u.gramr_models.Subdivision).render()}
</div>
