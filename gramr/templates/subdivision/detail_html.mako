<%inherit file="../${context.get('request').registry.settings.get('clld.app_template', 'app.mako')}"/>
<%namespace name="util" file="../util.mako"/>
<% import markdown%>
<%! active_menu_item = "subdivisions" %>
<%block name="title">${ctx.name}</%block>

You are here:
% if ctx.level > 1:
${h.link(request, ctx.chapter)} >
% endif
% if ctx.level > 2:
${h.link(request, ctx.section)} >
% endif
${ctx.name}

<article>

<h2>
	${ctx.name}
</h2>

% if ctx.description or ctx.markup_description:
${h.Markup(
		markdown.markdown(ctx.markup_description, extensions=['tables'])
	)
}
% endif

</article>

% if len(ctx.sections) + len(ctx.subsections) > 0:
<div>
	${request.get_datatable('subdivisions', u.gramr_models.Subdivision, subdivision=ctx).render()}
</div>
% endif