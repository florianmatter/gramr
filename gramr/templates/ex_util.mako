<%! from itertools import chain %>
<%def name="sentences(obj=None, fmt='long')">
	<%import gramr.util as util%>
    <% obj = obj or ctx %>
    <ul id="sentences-${obj.pk}" class="unstyled">
        % for a in obj.sentence_assocs:
            <li>
                <blockquote style="margin-top: 5px;">
				<% print(type(a.sentence)) %>
				% for file in a.sentence._files:
					% if file.mime_type.startswith('audio'):
						<% my_file = u.get_audio_link(file) %>
					% endif
				% endfor
                    ${h.link(request, a.sentence, label='(%s)' % (a.sentence.id))}
                    % if a.description and fmt == 'long':
                        <p>${a.description}</p>
                    % endif
                    ${util.rendered_sentence(a.sentence, fmt=fmt)}
                    % if (a.sentence.references or a.sentence.source) and fmt == 'long':
                        (${h.linked_references(request, a.sentence)|n}) <span class="muted"></span>
                    % endif
                </blockquote>
            </li>
        % endfor
    </ul>
</%def>