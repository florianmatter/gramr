import csv
folder = "vamale_source_data/"
reader = csv.DictReader(open(folder + "vamale_texts_from_flex.csv"))
example_data = {}
speakers = {
    "Jean": "jr",
    "Nini Pauty": "np",
    "Bealo Pauty": "bp",
    "": ""
}

for row in reader:
    ex_id = row["Example_ID"]
    example_data[ex_id] = row
    row["Speaker"] = speakers[row["Speaker"]]



with open ("./vamale_texts_edited.csv", "w") as csvfile:
    headers = example_data["PE1-1"].keys()
    writer = csv.DictWriter(csvfile, quoting=csv.QUOTE_ALL, fieldnames=headers)
    writer.writeheader()
    for example in example_data.values():
        writer.writerow(example)