Vamale has five vowel qualities, all of which can occur both short and long, and oral and nasalized.

![Vamale vowel diagram](/static/vamale_vowels.png)

Here is an example of a word-final contrast between /a/ and /aː/: morph_a:nigaa 'river shrimp' vs morph_a:kuha 'blowpipe'.
It is currently unclear whether vowel length interacts with stress and nasalization.
Another open question is the phonemic status of /ũ/; it only appears after aspirated consonants and after nasals.
However, [kʰ] only occurs before nasal vowels, so the decision between a phonemic status of either [ũ] or [kʰ] is not straightforward.

The mid-open vowels have allophones depending on the syllable structure.
Open syllables feature the more close allophones [e] and [o].
Closed short syllables will feature the more open vowels [ɛ] and [ɔ].
Closed long syllables feature both pairs, open vowels with final plosives, and closer vowels when ending in a nasal.
/a/ shows quite a large range of allophones, most prominently central [ə], but also [œ] when preceding /w/.