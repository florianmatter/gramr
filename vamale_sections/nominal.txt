This is the section on nominal morphology.
Vamale distinguishes between alienable and inalienable possession.
The latter type is marked with suffixes, two of which are illustrated below: ex:PE1-45 ex:PE1-99
exref:PE1-45 features the first person exclusive dual suffix obj:-bu.
exref:PE1-99 features the second person plural suffix obj:-vwe.

Alienable possessive suffixes (sometimes length changesː morph:nya1+morph:ko2+morph:n = morph:nyakoonyako 'for')

&nbsp;	|1INCL|	1EXCL|	2|	3 |
------|------|------|------|------|
SG|		|(e)ong|-go| -ea
DU|	gasu| 	abu|	gau|	lu
PL|	gase|	abe|	gavwe|	le

Alienable morphemes following open syllables take an initial obj:-n, diachronically a possessum marker (still present as morph:n). The marker was followed by a free pronoun, but phonological evidence, as well as the 1SG and 3SG forms suggest that the pronouns have become suffixes.

**Inalienable possessive suffixes**

&nbsp;	|1INCL|	1EXCL|	2|	3 |
------|------|------|------|------|
SG|	|	-ong|	-m|	-n
DU|	-ju|	-bu|	-u|	-lu
PL|	-je|	-be| 	-vwe|	-le


Alienable possession is marked differently from inalienable, although the markers are similar: ex:PE1-66 ex:PE1-26
Both exref:PE1-66 and exref:PE1-26 contain nouns with a first person possessor, 'my lunchbox' and 'my brothers', respectively.
However, 'lunchbox' is possessed alienably, taking first person morph:neongeong, while 'brother' is inalienably possessed, employing first person obj:-ng.

Apart from possessive morphology, Vamale nouns can bear nominalizing prefixes. The agentive nominalizer morph:xa1 is probably derived from the word morph:xayu 'man'. The locative nominalizer morph:ape- is derived from the bound root obj:ape 'trace', and can also be used for more generic functions such as 'fact of having done something', e.g. in morph:ape morph:hnyimake1 morph:aman 'NMLZR-think-something; idea'

&nbsp;	| |
------|------|
morph:xa1	|	agentive NMLZR
morph:hun	|	manner NMLZR
morph:ape	|	(locative) NMLZR
morph:e1	|	instrumental NMLZR
