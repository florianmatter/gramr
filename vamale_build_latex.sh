python3 gramr/scripts/create_latex_version.py
latexmk --xelatex latex_version.tex
mv latex_version.pdf gramr/static/download/latex_version.pdf
latexmk -C latex_version.tex
rm pynterlinear.log